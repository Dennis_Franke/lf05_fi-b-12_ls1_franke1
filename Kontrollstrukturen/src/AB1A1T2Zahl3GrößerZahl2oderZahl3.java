import java.util.Scanner;

public class AB1A1T2Zahl3Gr��erZahl2oderZahl3 {

	public static void main(String[] args) {
		
		int zahl1;
		int zahl2;
		int zahl3;
		
		Scanner scn = new Scanner(System.in);
		
		System.out.println("Geben Sie die erste Zahl ein:");
		zahl1 = scn.nextInt();
		
		System.out.println("Geben Sie die zweite Zahl ein:");
		zahl2 = scn.nextInt();
		
		System.out.println("Geben Sie die dritte Zahl ein:");
		zahl3 = scn.nextInt();
		
		scn.close();
		
		if (zahl3 > zahl2 || zahl3 > zahl1) {
			System.out.println("Die dritte Zahl ist gr��er als die zweite oder erste Zahl.");
		}
	}

}
