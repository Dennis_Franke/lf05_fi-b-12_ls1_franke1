import java.util.Scanner;

public class AB1A5BMI {

	public static void main(String[] args) {
		
		double koerpergewicht;
		double koerpergroeße;
		double bmi;
		String geschlecht;
		
		Scanner scn = new Scanner(System.in);
		
		System.out.println("Geben Sie Ihr Körpergewicht in kg ein: ");
		koerpergewicht = scn.nextDouble();
		
		System.out.println("Geben Sie Ihre Körpergröße in m ein: ");
		koerpergroeße = scn.nextDouble();
		
		System.out.println("Sind Sie männlich oder weiblich? (m/w)");
		geschlecht = scn.next();
		
		bmi = koerpergewicht / (koerpergroeße * koerpergroeße);
		
		scn.close();
		
//		Test-Code
		
//		System.out.println("bmi:");
//		bmi = scn.nextDouble();
		
		if (bmi < 20 && geschlecht.equals("m") || bmi < 19 && geschlecht.equals("w")) {
			System.out.println("Sie haben Untergewicht!");
			
		} else if (bmi >= 20 && bmi <= 25 && geschlecht.equals("m") || bmi >= 19 && bmi <= 24 && geschlecht.equals("w")) {
			System.out.println("Sie haben Normalgewicht.");
			
		} else {
			System.out.println("Sie haben Übergewicht!");
		}
		
	}

}
