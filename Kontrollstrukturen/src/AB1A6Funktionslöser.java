import java.util.Scanner;

public class AB1A6Funktionslöser {

	public static void main(String[] args) {
		
		double x;
		double funktionsWert = 0;
		String funktionsArt = "";
		
		Scanner scn = new Scanner(System.in);
		
		System.out.println("Geben Sie 'x' an: ");
		x = scn.nextDouble();
		
		scn.close();
		
		if (x <= 0) {
			
			funktionsWert = Math.pow(2.718 , x );
			funktionsArt = "exponentiell";	
			
		} else if (0 < x && x <= 3) {
			
			funktionsWert = Math.pow(x, 2) + 1;
			funktionsArt = "quadratisch";
			
		} else {
			
			funktionsWert = 2 * x + 4;
			funktionsArt = "linear";
			
		}
		
		System.out.println("Der Funktionswert ist: " + funktionsWert);
		System.out.println("Es handelt sich um eine " + funktionsArt + "e Funktion.");

	}

}
