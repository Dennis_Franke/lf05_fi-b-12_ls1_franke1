 import java.util.Scanner;

 public class Fahrkartenautomat_Endlosschleife {

	 	
	 	public static void main(String[] args) {
	 		
	 		for (int i = 1; i > 0; i++) {

	 			double gesamtpreis= fahrkartenbestellungErfassen ("Ihre Auswahl: ");
	 			double rückgabebetrag = fahrkartenBezahlen(gesamtpreis);

	 		if(rückgabebetrag > 0.0)
	 		{ 
	 			rueckgeldAusgeben (rückgabebetrag);
	 		}
	 		else
	 		{
	 			fahrkartenAusgeben();
	 		}
	 		System.out.println("");
	 		
	 	}
	     
	 	}

	 	public static double fahrkartenbestellungErfassen (String eingabe) {
	 		 Scanner tastatur = new Scanner(System.in);
	 	     double einzelpreis = 0;
	 	     double gesamtpreis = 0;
	 	     double anzahltickets = 0;
	 	     double ticketart = 0;
	 	     
	 	     do { 
	 	     
	 		 System.out.println("Wählen Sie ihre Wunschkarte für Berlin AB aus:");
	 		 System.out.println("Einzelfahrschein Regeltarif AB [2,90 Euro] (1)");
	 		 System.out.println("Tageskarte Regeltarif AB [8,60 Euro] (2)");
	 		 System.out.println("Kleingruppen-Tageskarte Regeltarif AB [23,50 Euro] (3)");
	 		 System.out.println("Bezahlen (9)");
	 		 System.out.println("");
	 		 System.out.print(eingabe);
	 		 ticketart = tastatur.nextDouble();
	 		 
	 	    while (ticketart >=4 && ticketart != 9 || ticketart <=0) {
	 	    	 System.out.println(" >>falsche Eingabe<<");
	 	    	 System.out.println("Bitte wiederholen sie ihre Auswahl:");
	 	    	 ticketart = tastatur.nextDouble();
	 	    	 }
	 	    
	 	    if (ticketart == 1) {
	 	    	 einzelpreis = 2.90;
	 	     }
	 	    
	 	    else if (ticketart == 2) {
	 	    	 einzelpreis = 8.60;
	 	     }
	 	    
	 	    else if (ticketart == 3) {
	 	    	 einzelpreis = 23.50;
	 	}
	 	    
	 	    if (ticketart != 9) {
	 	    System.out.print("Anzahl der Tickets: ");
	 	     anzahltickets = tastatur.nextDouble();
	 	     while (anzahltickets<= 0 || anzahltickets >= 11) {

	 	 			System.out.println ("Es gab einen Fehler bei der eingegebenen Ticketanzahl."

	 				+ "\n >> Wählen Sie bitte eine Anzahl von 1 bis 10 Tickets aus <<");
	 	 			System.out.println("Anzahl der Tickets: ");
	 	 			anzahltickets=tastatur.nextDouble ();

	 	 		}
	 	     double preis= einzelpreis * anzahltickets;
	 	     gesamtpreis = gesamtpreis + preis;
	 	     System.out.println("Zwischensumme: " + gesamtpreis + " Euro");
	 	    }
	 	    
	 	     for (int i = 0; i < 3; i++)
	 		       {
	 	    	 System.out.println ("");
	 		          try {
	 					Thread.sleep(100);
	 				} catch (InterruptedException e) {
	 					// TODO Auto-generated catch block
	 					e.printStackTrace();
	 				}
	 		       }
	 	     
	 	    } while (ticketart != 9); 
	 	     return gesamtpreis;
	 	    
	 	}

	 	public static double fahrkartenBezahlen(double preis) {
	 		
	 		double zuZahlenderBetrag = preis;
	 		System.out.printf("\n%s %.2f %s\n", "Der zu zahlende Betrag ist ", zuZahlenderBetrag, " Euro");
	 		Scanner tastatur = new Scanner(System.in);
	 	      double eingezahlterGesamtbetrag = 0.0;
	 	      double rückgabebetrag = 0.0;
	 	      
	 	       while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
	 	       {
	 	    	   System.out.printf("\n%s %.2f %s\n" ,"Noch zu zahlen: ", (zuZahlenderBetrag - eingezahlterGesamtbetrag), "Euro");
	 	    	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
	 	    	   double eingeworfeneMünze = tastatur.nextDouble();
	 	           eingezahlterGesamtbetrag += eingeworfeneMünze;
	 	           rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
	 	       }
	 	       
	 	       return rückgabebetrag;
	 	}
	 	
	 	public static void fahrkartenAusgeben () {
	 		 System.out.println("\nFahrscheine werden ausgegeben");
	 		 
	 	       for (int i = 0; i < 8; i++)
	 	       {
	 	          System.out.print("=");
	 	          try {
	 				Thread.sleep(250);
	 			} catch (InterruptedException e) {
	 				// TODO Auto-generated catch block
	 				e.printStackTrace();
	 			}
	 	       }
	 	       
	 	       System.out.println("\n");
	 	       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
	                   "vor Fahrtantritt entwerten zu lassen!\n"+
	                   "Wir wünschen Ihnen eine gute Fahrt.");
	 	       
	 	       for (int i = 0; i < 8; i++)
	 	       {
	 	          System.out.println("");
	 	          try {
	 				Thread.sleep(250);
	 			} catch (InterruptedException e) {
	 				// TODO Auto-generated catch block
	 				e.printStackTrace();
	 			}
	 	       }
	 	       
	 	       }
	 	
	 	public static void rueckgeldAusgeben (double rueckgeld) {
	 			 System.out.println("\nFahrschein wird ausgegeben");
	 			 
	 		       for (int i = 0; i < 8; i++)
	 		       {
	 		          System.out.print("=");
	 		          try {
	 					Thread.sleep(250);
	 				} catch (InterruptedException e) {
	 					// TODO Auto-generated catch block
	 					e.printStackTrace();
	 				}
	 		       }
	 		       
	 		       System.out.println("\n");
	 	    	   System.out.printf("\n%s %.2f %s\n" ,"Der Rückgabebetrag in Höhe von ", rueckgeld, " EURO");
	 	    	   System.out.println("wird in folgenden Münzen ausgezahlt:");

	 	           while(Math.round(rueckgeld*100.0)/100.0 >= 2.0) // 2 EURO-Münzen
	 	           {
	 	        	  System.out.println("2 EURO");
	 		          rueckgeld -= 2.0;
	 	           }
	 	           
	 	           while(Math.round(rueckgeld*100.0)/100.0 >= 1.0) // 1 EURO-Münzen
	 	           {
	 	        	  System.out.println("1 EURO");
	 		          rueckgeld -= 1.0;
	 	           }
	 	           
	 	           while(Math.round(rueckgeld*100.0)/100.0 >= 0.5) // 50 CENT-Münzen
	 	           {
	 	        	  System.out.println("50 CENT");
	 		          rueckgeld -= 0.5;
	 	           }
	 	           
	 	           while(Math.round(rueckgeld*100.0)/100.0 >= 0.2) // 20 CENT-Münzen
	 	           {
	 	        	  System.out.println("20 CENT");
	 	 	          rueckgeld -= 0.2;
	 	           }
	 	           
	 	           while(Math.round(rueckgeld*100.0)/100.0 >= 0.1) // 10 CENT-Münzen
	 	           {
	 	        	  System.out.println("10 CENT");
	 		          rueckgeld -= 0.1;
	 	           }
	 	           
	 	           while(Math.round(rueckgeld*100.0)/100.0 >= 0.05)// 5 CENT-Münzen
	 	           {
	 	        	  System.out.println("5 CENT");
	 	 	          rueckgeld -= 0.05;
	 	           }
	 	           for (int i = 0; i < 2; i++)
	 		       {
	 		          try {
	 					Thread.sleep(250);
	 				} catch (InterruptedException e) {
	 					// TODO Auto-generated catch block
	 					e.printStackTrace();
	 				}
	 		       }
	 	           
	 	           System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
	                       "vor Fahrtantritt entwerten zu lassen!\n"+
	                       "Wir wünschen Ihnen eine gute Fahrt.");
	 	           
	 	           for (int i = 0; i < 8; i++)
	 		       {
	 		          System.out.println("");
	 		          try {
	 					Thread.sleep(250);
	 				} catch (InterruptedException e) {
	 					// TODO Auto-generated catch block
	 					e.printStackTrace();
	 				}
	 		       }
	 	           
	 	}
	 	
	 }
	