public class Argumente { 
 public static void main(String[] args) { 
  ausgabe(1, "Mana"); 
  ausgabe(2, "Elise"); 
  ausgabe(3, "Johanna"); 
  ausgabe(4, "Felizitas"); 
  ausgabe(5, "Karla"); 
  System.out.println(vergleichen(1, 2)); 
  System.out.println(vergleichen(1, 5)); 
  System.out.println(vergleichen(3, 4)); 
 } 
 
 public static void ausgabe(int zahl, String name) { 
  System.out.println(zahl + ": " + name); 
 } 
 
 public static boolean vergleichen(int arg1, int arg2) { 
  return (arg1 + 8) < (arg2 * 3); 
 }
}
// Es wird geschaut 1+8 kleiner als 2*3 , 1+8 kleiner als 5*3 und 3+8 kleiner als 4*3 sind.
// 9 ist gr��er als 6 also wir "false" ausgegeben, 9 ist kleiner als 15 also wird "true"ausgegeben. 11 ist kleiner als 12 also wird "true" ausgegeben.