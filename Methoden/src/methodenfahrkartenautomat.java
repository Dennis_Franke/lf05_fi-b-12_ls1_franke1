import java.util.Scanner;
public class methodenfahrkartenautomat {
	
	public static void main(String[] args) {
		double preis= fahrkartenbestellungErfassen ("Der Ticketpreis ist: ");
		double anzahl= fahrkartenbestellungErfassen ("Die Ticketanzahl ist: ");
		double rückgabebetrag = fahrkartenBezahlen(preis, anzahl);
		if(rückgabebetrag > 0.0)
		{ 
			rueckgeldAusgeben (rückgabebetrag);
		}
		else
		{
			fahrkartenAusgeben();
		}
    
	}
	public static double fahrkartenbestellungErfassen (String eingabe) {
		 Scanner tastatur = new Scanner(System.in);
		 System.out.print(eingabe);
	     double Zahl1 = tastatur.nextDouble();
	     return Zahl1;
	}
	
	public static double fahrkartenBezahlen(double x, double y) {
		double zuZahlenderBetrag = x * y;
		System.out.printf("\n%s %.2f %s\n", "Der zu zahlende Betrag ist ", zuZahlenderBetrag, " Euro");
		Scanner tastatur = new Scanner(System.in);
	      double eingezahlterGesamtbetrag = 0.0;
	      double rueckgabebetrag = 0.0;
	       while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
	       {
	    	   System.out.printf("\n%s %.2f %s\n" ,"Noch zu zahlen: ", (zuZahlenderBetrag - eingezahlterGesamtbetrag), "Euro");
	    	   System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
	    	   double eingeworfeneMünze = tastatur.nextDouble();
	           eingezahlterGesamtbetrag += eingeworfeneMünze;
	           rueckgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
	       }
	       	return rueckgabebetrag;
	}
	
	public static void fahrkartenAusgeben () {
		 System.out.println("\nFahrschein wird ausgegeben");
	       for (int i = 0; i < 8; i++)
	       {
	          System.out.print("=");
	          try {
				Thread.sleep(250);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	       }
	       System.out.println("\n");
	       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                  "vor Fahrtantritt entwerten zu lassen!\n"+
                  "Wir w�nschen Ihnen eine gute Fahrt.");
	}
	
	public static void rueckgeldAusgeben (double z) {
			 System.out.println("\nFahrschein wird ausgegeben");
		       for (int i = 0; i < 8; i++)
		       {
		          System.out.print("=");
		          try {
					Thread.sleep(250);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		       }
		       System.out.println("\n");
	    	   System.out.printf("\n%s %.2f %s\n" ,"Der R�ckgabebetrag in H�he von ", z, " EURO");
	    	   System.out.println("wird in folgenden M�nzen ausgezahlt:");

	           while(z >= 2.0) // 2 EURO-M�nzen
	           {
	        	  System.out.println("2 EURO");
		          z -= 2.0;
	           }
	           while(z >= 1.0) // 1 EURO-M�nzen
	           {
	        	  System.out.println("1 EURO");
		          z -= 1.0;
	           }
	           while(z >= 0.5) // 50 CENT-M�nzen
	           {
	        	  System.out.println("50 CENT");
		          z -= 0.5;
	           }
	           while(z >= 0.2) // 20 CENT-M�nzen
	           {
	        	  System.out.println("20 CENT");
	 	          z -= 0.2;
	           }
	           while(z >= 0.1) // 10 CENT-M�nzen
	           {
	        	  System.out.println("10 CENT");
		          z -= 0.1;
	           }
	           while(z >= 0.05)// 5 CENT-M�nzen
	           {
	        	  System.out.println("5 CENT");
	 	          z -= 0.05;
	           }
	           System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                      "vor Fahrtantritt entwerten zu lassen!\n"+
                      "Wir w�nschen Ihnen eine gute Fahrt.");
	}
	
}