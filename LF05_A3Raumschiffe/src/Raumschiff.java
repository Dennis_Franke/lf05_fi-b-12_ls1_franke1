import java.util.ArrayList;

public class Raumschiff {
	
	private String schiffname;
	private double energieversorgung;
	private double schutzschilde;
	private double lebenserhaltungssysteme;
	private double huelle; 
	private int photonentorpedos;
	private int reperaturdroiden;
	private ArrayList<Ladung> ladung = 
			new ArrayList();

	
	public Raumschiff(String schiffname, double energieversorgung, double schutzschilde, double lebenserhaltungssysteme, double huelle, int photonentorpedos, int reperaturdroiden){
		this.schiffname = schiffname;
		this.energieversorgung = energieversorgung;
		this.schutzschilde = schutzschilde;
		this.lebenserhaltungssysteme = lebenserhaltungssysteme;
		this.huelle = huelle;
		this.photonentorpedos = photonentorpedos;
		this.reperaturdroiden = reperaturdroiden;
		this.ladung = ladung; 
	
	}

	
	public String getSchiffname() {
		return schiffname;
	}
	public void setSchiffname(String schiffname) {
		this.schiffname = schiffname;
	}
	public double getEnergieversorgung() {
		return energieversorgung;
	}
	public void setEnergieversorgung(double energieversorgung) {
		this.energieversorgung = energieversorgung;
	}
	public double getSchutzschilde() {
		return schutzschilde;
	}
	public void setSchutzschilde(double schutzschilde) {
		this.schutzschilde = schutzschilde;
	}
	public double getLebenserhaltungssysteme() {
		return lebenserhaltungssysteme;
	}
	public void setLebenserhaltungssysteme(double lebenserhaltungssysteme) {
		this.lebenserhaltungssysteme = lebenserhaltungssysteme;
	}
	
	public double getHuelle() {
		return huelle;
	}
	
	public void setHuelle(double huelle) {
		this.huelle = huelle;
	}
	
	public int getPhotonentorpedos() {
		return photonentorpedos;
	}
	
	public void setPhotonentorpedos(int photonentorpedos) {
		this.photonentorpedos = photonentorpedos;
	}
	
	public int getReperaturdroiden() {
		return reperaturdroiden;
	}
	
	public void setReperaturdroiden(int reperaturdroiden) {
		this.reperaturdroiden = reperaturdroiden;
	} 
	
	public void addLadung(Ladung il) {
		ladung.add(il);
	}
	public void photonentorpedosAbschießen(String zielRaumschiff) {
	
	}
	
	public void phaserkanonenAbschießen(String zielRaumschiff) {
	
	}
	
	private void genommenerSchaden(int treffer, double ausmassSchaden) {

	}
	
	public void nachrichtSenden(String nachricht){
		
	}
	public void beladen(String aufzuladeneLadung) {
		
	}
	public void logbuchZureuckgeben(){
		
	}
	public void photonentopedosLaden(int anzahlTorpedos) {
		
	}
	public void reperaturAuftrag (boolean schutzschilde, boolean energieversorgung, boolean schiffshülle, int anzahlDroiden) {
		
	}
	public void zustandRaumschiff () {
		
		
	}
	
	public void ausgabeLadungsverzeichnis() {
		System.out.println();
		System.out.println(" Das Raumschiff " + schiffname + " besitzt folgende Ladung: ");
		for (int i = 0; i < ladung.size(); i++) {
			System.out.println(ladung.get(i));
		}
		
	}
	public void aufräumenLadungsverzeichnis() {
		
	}
}


