
public class Test {

	public static void main(String[] args) {
		Ladung il  = new Ladung("Borg-Schrott","5");
		Ladung il1 = new Ladung("Rote Materie", "2");
		Ladung il2 = new Ladung("Plasma-Waffe","50");
		Ladung il3 = new Ladung("Ferengi-Schneckensaft","200");
		Ladung il4 = new Ladung("Bat'leth Klingonen-Schwert", "200");
		Ladung il5 = new Ladung("Photonentorpedo","3");
		Ladung il6 = new Ladung("Forschungssonde", "3");
		
		
		Raumschiff r1 = new Raumschiff( "IKS Hegh'ta" , 100,100,100,100,1,2);
		Raumschiff r2 = new Raumschiff( "IRW Khazara" , 100,100,100,100,2,2);
		Raumschiff r3 = new Raumschiff( "Ni'Var" , 80,80,100,50,0,5);
		
		r1.addLadung(il3);
		r1.addLadung(il4);
		r1.ausgabeLadungsverzeichnis();
		
		r2.addLadung(il);
		r2.addLadung(il1);
		r2.addLadung(il2);
		r2.ausgabeLadungsverzeichnis();
		
		r3.addLadung(il6);
		r3.addLadung(il5);
		r3.ausgabeLadungsverzeichnis();
		
		r1.zustandRaumschiff();
		// r1.ausgabeLadungsverzeichnis();
	}

}
