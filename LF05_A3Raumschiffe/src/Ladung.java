
public class Ladung {

		private String ladungsart;
		private String anzahl;
		
		//Konstruktor
		 
		
		public Ladung(String ladungsart, String anzahl) {
			this.ladungsart = ladungsart;
			this.anzahl = anzahl;
			
		}

		public String getLadungsart() {
			return ladungsart;
		}

		public void setLadungsart(String ladungsart) {
			this.ladungsart = ladungsart;
		}

		public String getAnzahl() {
			return anzahl;
		}

		public void setAnzahl(String anzahl) {
			this.anzahl = anzahl;
		}
		
		public String toString() {
			return this.anzahl + " " + ladungsart;
			
		}
		
	}
